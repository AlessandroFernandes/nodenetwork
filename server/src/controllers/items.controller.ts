import { Request, Response } from 'express';
import knex from '../database/connection';

import Item from '../models/Item';

class ItemsController 
{
    async show(req: Request , res: Response)
    {
        try
              {
            const items: Item[] = await knex('items').select('*');
            const serializedItems: Item[] = items.map(( item:Item )  => ({
                id: item.id,
                name: item.name,
                image: "http://localhost:9000/upload/" + item.image
            }));
            res.status(200).json(serializedItems);
        }
        catch(e)
        {
            res.status(401).json({ Error: e.message})
        } 
    }
}

export default ItemsController;