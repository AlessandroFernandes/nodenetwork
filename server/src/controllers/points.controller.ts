import {Request, Response} from 'express';
import knex from '../database/connection';

import Item from '../models/Item';
import Point from '../models/point';

class PointsController
{
    async create(req: Request, res: Response)
    {
        try
        {
            const response: Point = await req.body;
            
            const trx = await knex.transaction();
    
            const point = 
                        { 
                           image    : response.image,
                           whatsapp : response.whatsapp,
                           latitude : response.latitude,
                           longitude: response.longitude,
                           email    : response.email,
                           name     : response.name,
                           city     : response.city,
                           uf       : response.uf 
                        };
            
            const idPost = await trx('point').insert(point);
            
            const point_item = response.items.map(item => ({
                id_item : item,
                id_point: idPost[0]
            }));
    
            await trx('point_item').insert(point_item);
            
            trx.commit();
            
            response.id = idPost[0];
            res.status(201).json(response);
        }
        catch(e)
        {
            console.log(e.message);
            res.status(400).json({Error: "Error internal"})
        }
    }

    async show(req: Request, res: Response)
    {
        const idParam = Number(req.params.id);

        const point = await knex('point').where('id', idParam).first() as unknown as Point;

        if(!point) res.status(400).json({'Error': 'Params error'});

        const items = await knex('items')
                      .join('point_item', 'items.id', '=', 'point_item.id_item')
                      .where('point_item.id_point', idParam)
                      .select('items.id', 'items.name', 'items.image') as unknown as Item[];
        
        const serializedItems: Item[] = items.map(item => ({
                                id: item.id,
                                name: item.name,
                                image: 'http://localhost:9000/upload/' + item.image.toString()
                            }));

        point.items = serializedItems;

        res.status(200).json(point);
    }

    async  index(req: Request, res: Response)
    {
        const { city, uf, items } = req.query;

        if(!city || !uf || !items) res.status(400).json({"Error": "Params error"});

        const itemsQuery = String(items).split(',').map(item => Number(item.trim()));
       
        const point = await knex('point')
                            .join('point_item', 'point.id', '=', 'point_item.id_point')
                            .whereIn('point_item.id_item', itemsQuery)
                            .where('city', String(city))
                            .where('uf', String(uf))
                            .distinct()
                            .select('point.*') as unknown as Point;

        res.status(200).json(point);
    }
}

export default PointsController;