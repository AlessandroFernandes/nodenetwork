import express = require('express');
import path from 'path';
import cors from 'cors';
const router = require('./router');

const app = express();

app.use(cors());
app.use(express.json());
app.use(router);
app.use('/upload', express.static(path.resolve(__dirname, '..', 'upload')));

app.listen("9000", ()=> { console.log('**************** Server Started  - Port: 9000    *******************')})