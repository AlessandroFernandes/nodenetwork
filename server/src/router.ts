import express = require('express');
const router = express.Router();

import ItemsController from './controllers/items.controller';
import PointsController from './controllers/points.controller';

const itemsController = new ItemsController();
const pointsController = new PointsController();

router.get('/items', (req, res) => itemsController.show(req, res));

router.get('/points', (req, res) => pointsController.index(req, res));
router.get('/points/:id', (req, res) => pointsController.show(req, res))
router.post('/points', (req, res) => pointsController.create(req, res));

module.exports = router;

